import {Component, OnInit, ViewChild} from '@angular/core';
import * as wjMultirow from '@grapecity/wijmo.grid.multirow';
import * as wjcCore from '@grapecity/wijmo';
import * as wjcGrid from '@grapecity/wijmo.grid';
import {data} from "../../data";
import * as wjGrid from '@grapecity/wijmo.grid';

@Component({
  selector: 'app-grid',
  templateUrl: './grid.component.html',
  styleUrls: ['./grid.component.css']
})
export class GridComponent implements OnInit {
  dataSource = data.Table1;
  bIsAnimated = true;
  columnGroups = groupHeader;
  data: wjcCore.CollectionView | undefined;
  dataTotal: any = [];
  @ViewChild('flex') flex: wjcGrid.FlexGrid | undefined;
  @ViewChild('groupMultirow', { static: true }) groupMultirow: wjMultirow.MultiRow | undefined;

  constructor() { }

  ngOnInit(): void {
    this.handeData();
  }

  handeData(){
    const data: any = this.dataSource;
    data.forEach((item: any) => {
      const dDate = new Date(item.DocDate);
      item.dateGeneral = `${dDate.getDay() + 1}/${dDate.getMonth() + 1}/${dDate.getFullYear()}`;
    });
    const uniqueData = Array.from(new Set(data.map((item: any) => item.dateGeneral)));
    uniqueData.forEach((item: any) => {
      const dataFilterByDate = data.filter((element: any) => element.dateGeneral === item);
      const nTotal = this.sum('Amount', dataFilterByDate);
      this.dataTotal[item] = nTotal;
    });

    this.data = new wjcCore.CollectionView(data, {
      groupDescriptions: [ 'dateGeneral']
    });
  }

  sum(pzKey: string, pData: any[]){
    const nAmount = pData.map((item: any) => item[pzKey]).reduce((nTotal: number, nItem: number) => {
        return Number(nTotal) + Number(nItem);
      });
    return nAmount;
  }

  initializeGrid(grid: wjcGrid.FlexGrid) {
    grid.select(new wjcGrid.CellRange(3, 3), true);
  }

  onloadedRows(grid: wjGrid.FlexGrid) {
    for (var i = 0; i < grid.rows.length; i++) {
      var row = grid.rows[i];
      var item = row.dataItem;
      if (item._name && this.dataTotal[item._name] >= 1000000000) {
        row.cssClass = 'high-value-1';
      }

      if (item._name && this.dataTotal[item._name] < 1000000000){
        row.cssClass = 'low-value';
      }
    }
  }
}

const groupHeader = [
  { header: 'Chứng từ', columns: [
      {binding: 'dateGeneral', header: 'Ngày', width: '10*'},
      {binding: 'DocNo', header: 'Số', width: '10*'},
  ]},
  {header: 'Diễn giải', binding: 'Description', width: '40*'},
  {header: 'Tài khoản', columns: [
      {header: 'Nợ', binding: 'DebitAmount', width: '10*'},
      {header: 'Có', binding: 'ExchangeRate', width: '10*'}
  ]},
  {header: 'Tiền', binding: 'Amount', width: '10*', aggregate: 'Sum'},
  {header: 'Tỷ giá', binding: 'CreditAmount', width: '10*'},

];
