import {Component, forwardRef, Input, OnInit} from '@angular/core';
import {
  AbstractControl,
  ControlValueAccessor,
  NG_VALIDATORS,
  NG_VALUE_ACCESSOR,
  ValidationErrors,
  Validator
} from "@angular/forms";

@Component({
  selector: 'app-input-login',
  templateUrl: './input-login.component.html',
  styleUrls: ['./input-login.component.css'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      multi:true,
      useExisting: forwardRef(() => InputLoginComponent)
    },
    {
      provide: NG_VALIDATORS,
      useExisting: forwardRef(() => InputLoginComponent),
      multi: true
    }
  ]
})
export class InputLoginComponent implements OnInit, ControlValueAccessor, Validator{
  @Input() zType : 'text' | 'password' | 'email' |  undefined;
  @Input() messageValidate: {zType: string, zMessage: string}[] | undefined;
  @Input() bIsReadonly: boolean | undefined
  zValueCurrent = '';
  onChange: any | undefined;
  onTouched: any | undefined;
  formControl: any | null;
  bIsDisabled = false;
  constructor() { }
  ngOnInit(): void {
    if (!this.messageValidate) {
      this.messageValidate = this.zType == 'password' ? messageValidate.password: this.zType == 'email' ? messageValidate.email : messageValidate.text;
    }
  }

  handleValue(event: any){
    const zValue = event.target.value;
    this.onChange(zValue);
  }

  writeValue(pzValue: string){
    this.zValueCurrent = pzValue;
  }

  registerOnChange(fn: (event: any) => void) {
    this.onChange = fn
  }

  registerOnTouched(fn: (event: any) => void) {
    this.onTouched = fn;
  }

  setDisabledState(pbIsDisabled: boolean) {
    this.bIsDisabled = pbIsDisabled;
  }

  validate(control: AbstractControl): any{
    this.formControl = control;
  }
}

const messageValidate = {
  text: [
    {zType: 'required', zMessage: 'Không được để trống'},
  ],
  email: [
    {zType: 'required', zMessage: 'Email không được để trống'},
    {zType: 'email', zMessage: 'Không đúng định dạng email'},
  ],
  password: [
    {zType: 'required', zMessage: 'Mật khẩu không được để trống'},
    {zType: 'password', zMessage: 'Mật khẩu không được để trống'},
  ]
}
